﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefRigidbody/Rigidbody")]
public class RefRigidbody : RefVariable<Rigidbody>{}
