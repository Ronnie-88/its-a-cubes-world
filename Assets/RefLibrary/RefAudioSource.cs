﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefAudioSource/AudioSource")]
public class RefAudioSource : RefVariable<AudioSource>{}

