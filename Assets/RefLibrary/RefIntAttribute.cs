﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefInt/Int Attribute")]
public class RefIntAttribute : RefInt
{
    [SerializeField]
    [Tooltip("Maximium Value your attribute can reach")]
    private int _Max;
    [SerializeField]
    [Tooltip("Minimium Value your attribute can reach")]
    private int _Min;
    public event Action<int> NotifyOnValueIncreased;
    public event Action<int> NotifyOnValueDecreased;

    public new int Value
    {
        get {return _Value;}
        set
        {
            if (value > _Value && value <= _Max)
            {
                NotifyOnValueIncreased?.Invoke(value);
            }
            else if(value < _Value && value >= _Min)
            {
                NotifyOnValueDecreased?.Invoke(value);
            }

            _Value = Mathf.Clamp(value, _Min, _Max);
        }
    }

    

}
