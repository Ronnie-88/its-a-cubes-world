﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RefVariables/RefPlayerInput/PlayerInput")]
public class RefPlayerInput : RefVariable<PlayerInput>
{}
