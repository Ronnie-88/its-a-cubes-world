﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefInt/Int")]
public class RefInt : RefVariable<int>{}
