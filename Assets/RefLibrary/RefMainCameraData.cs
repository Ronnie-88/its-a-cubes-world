﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RefVariables/RefMainCameraData/MainCameraData")]
public class RefMainCameraData : RefVariable<MainCameraData>
{
}
