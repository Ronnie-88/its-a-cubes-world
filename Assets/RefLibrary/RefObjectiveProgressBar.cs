using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RefVariables/RefObjectiveProgressBar/UIObjectiveProgressBar")]
public class RefObjectiveProgressBar : RefVariable<UIObjectiveProgressBar>
{
}
