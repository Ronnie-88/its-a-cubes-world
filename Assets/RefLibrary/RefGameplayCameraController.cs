﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RefVariables/RefGameplayCameraController/GameplayCameraController")]
public class RefGameplayCameraController : RefVariable<GameplayCameraController>
{
   
}
