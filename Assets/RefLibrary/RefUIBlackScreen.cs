﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RefVariables/RefUIBlackScreen/UIScreenBlackScreen")]
public class RefUIBlackScreen : RefVariable<UIScreenBlackScreen>
{

}
