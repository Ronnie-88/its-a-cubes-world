using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RefVariables/RefBool/Bool")]
public class RefBool : RefVariable<bool>
{
}
