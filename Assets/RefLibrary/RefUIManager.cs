using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RefVariables/RefUIManger/UIManager")]
public class RefUIManager : RefVariable<UIManager>
{
}
