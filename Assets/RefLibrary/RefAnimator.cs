﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefAnimator/Animator")]
public class RefAnimator : RefVariable<Animator>{}
