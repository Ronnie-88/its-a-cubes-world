﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefFloat/Float Attribute")]
public class RefFloatAttribute : RefFloat
{
    [SerializeField]
    [Tooltip("Maximium Value your attribute can reach")]
    private float _Max;
    [SerializeField]
    [Tooltip("Minimium Value your attribute can reach")]
    private float _Min;
    public event Action<float> NotifyOnValueIncreased;
    public event Action<float> NotifyOnValueDecreased;
    public float Max { get => _Max; set => _Max = value; }
    public float Min { get => _Min; set => _Min = value; }


    public new float Value
    {
        get {return _Value;}
        set
        {
            if (value > _Value && value <= Max)
            {
                _Value = Mathf.Clamp(value, Min, Max);
                NotifyOnValueIncreased?.Invoke(value);
            }
            else if(value < _Value && value >= Min)
            {
                _Value = Mathf.Clamp(value, Min, Max);
                NotifyOnValueDecreased?.Invoke(value);
            }
            _Value = Mathf.Clamp(value, Min, Max);
            
        }
    }


    public float GetFloatAttributePercent()
    {
        return (_Value - Min) / (Max - Min);
    }
}
