﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefRigidbody/Rigidbody2D")]
public class RefRigidbody2D : RefVariable<Rigidbody2D>
{}
