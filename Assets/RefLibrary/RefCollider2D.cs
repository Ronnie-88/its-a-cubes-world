﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefCollider/Collider2D")]
public class RefCollider2D : RefVariable<Collider2D>{}
