using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RefVariables/RefUIScreenMainMenu/UIScreenMainMenu")]
public class RefUIScreenMainMenu : RefVariable<UIScreenMainMenu>
{
}
