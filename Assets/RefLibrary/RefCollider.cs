﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefCollider/Collider")]
public class RefCollider : RefVariable<Collider>{}
