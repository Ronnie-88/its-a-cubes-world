﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RefVariable<T> : ScriptableObject
{
    protected T _Value;
    public event Action<T> NotifyOnValueChanged;


    public T Value
    {
        get { return _Value; }
        set
        {
            _Value = value;
            NotifyOnValueChanged?.Invoke(_Value);
        }
    }
   
}
