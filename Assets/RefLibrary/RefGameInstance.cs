using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RefVariables/RefGameInstance/GameInstance")]
public class RefGameInstance : RefVariable<GameInstance>
{
}
