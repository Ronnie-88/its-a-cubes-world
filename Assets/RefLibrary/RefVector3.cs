﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefVector/Vector3")]
public class RefVector3 : RefVariable<Vector3>
{}
