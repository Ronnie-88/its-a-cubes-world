﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefVector/Vector2")]
public class RefVector2 : RefVariable<Vector2>{}
