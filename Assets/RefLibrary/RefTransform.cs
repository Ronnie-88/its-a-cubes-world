﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RefVariables/RefTransform/Transform")]
public class RefTransform : RefVariable<Transform>{}
