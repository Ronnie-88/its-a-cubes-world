﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "RefVariables/RefFloat/Float")]
public class RefFloat : RefVariable<float>{}
