﻿using UnityEngine;

[ExecuteInEditMode]
public class CubeTileColorSwitcher : MonoBehaviour
{
    [SerializeField, ColorUsage(false, true)]
    private Color _MainColor;
    [SerializeField, ColorUsage(false, true)]
    private Color _InnerColor;
    [SerializeField, Range(0, 1)]
    private float _InnerSize;

    private MeshRenderer _MeshRenderer;
    private MaterialPropertyBlock _CurrentPropertyBlock;

    private void Awake()
    {
        _MeshRenderer = GetComponent<MeshRenderer>();
    }

    private void OnEnable()
    {
        SetMaterialInitialProperties();
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Application.isPlaying == false)
        {
           // SetMaterialInitialProperties();
        }
        //ChangeOutterCubeColour(_MainColor);
    }
#endif

    private void SetMaterialInitialProperties()
    {
        MaterialPropertyBlock newPropertyBlock = new MaterialPropertyBlock();
        newPropertyBlock.SetColor("_MainColor", _MainColor);
        newPropertyBlock.SetColor("_InnerSquareColor", _InnerColor);
        newPropertyBlock.SetFloat("_InnerSquareSize", _InnerSize);
        _MeshRenderer.SetPropertyBlock(newPropertyBlock);
        _CurrentPropertyBlock = newPropertyBlock;
    }

    public void ChangeOutterCubeColour(Color color)
    {
        MaterialPropertyBlock newPropertyBlock = _CurrentPropertyBlock;
        newPropertyBlock.SetColor("_MainColor", color);
        _MeshRenderer.SetPropertyBlock(newPropertyBlock);

    }

    public Color GetMainColour()
    {
        return _MainColor;
    }

    public void ChangeInnerSize(float percentage)
    {
        MaterialPropertyBlock newPropertyBlock = _CurrentPropertyBlock;
        newPropertyBlock.SetFloat("_InnerSquareSize", percentage);
        _MeshRenderer.SetPropertyBlock(newPropertyBlock);
    }
}
