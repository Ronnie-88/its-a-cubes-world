﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMove<T>
{
    void AddMotion(T motionComponent, Vector3 direction, float motionMagnitude);
}
