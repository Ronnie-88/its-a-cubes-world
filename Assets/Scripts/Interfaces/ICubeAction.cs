﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICubeAction
{
    void TriggerCubeAction();
}
