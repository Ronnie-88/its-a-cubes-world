﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField]
    private RefSoundManager _SoundManager;
    [SerializeField]
    private AudioClip _MainMenuClip;
    [SerializeField]
    private AudioClip _Level1;
    [SerializeField]
    private AudioClip _Level2;
    [SerializeField]
    private AudioClip _Level3;
    [SerializeField]
    private LevelLoadManager _LevelLoadManager;
    private AudioSource _AudioSource;
    private AudioClip _CurrentSound;
    public enum GameMusicState { ON, OFF }
    public enum SFXState { ON, OFF }
    private GameMusicState _CurrentMusicState = GameMusicState.ON;
    private SFXState _CurrentSFXState = SFXState.ON;
    public event Action NotifyOnToggleOn;
    public event Action NotifyOnToggleOff;

    private void Awake()
    {
        _SoundManager.Value = this;
        _AudioSource = GetComponent<AudioSource>();
        _CurrentSound = _MainMenuClip;
        _AudioSource.clip = _CurrentSound;
        _AudioSource.Play();

    }

    public void ToggleGameMusic()
    {
        switch (_CurrentMusicState)
        {
            case GameMusicState.ON:
                _AudioSource.volume = 0.0f;
                _CurrentMusicState = GameMusicState.OFF;
                break;
            case GameMusicState.OFF:
                _AudioSource.volume = 0.175f;
                _CurrentMusicState = GameMusicState.ON;
                break;
        }
    }


    public void PlayMainMenuTrack()
    {
        ClipChanger(_MainMenuClip);
    }
    public void PlayLevel1Track()
    {
        ClipChanger(_Level1);
    }
    public void PlayLevel2Track()
    {
        ClipChanger(_Level2);
    }
    public void PlayLevel3Track()
    {
        ClipChanger(_Level3);
    }

    private void ClipChanger(AudioClip clip)
    {
        if (Checkclip(clip))
        {
            return;
        }
        _CurrentSound = clip;
        _AudioSource.clip = _CurrentSound;
        _AudioSource.Play();
    }

    private bool Checkclip(AudioClip clip)
    {
        return _AudioSource.clip == clip;
    }

    private void SoundSwitcher(int index)
    {
        bool IsLevel1 = (index >=2) && (index <= 4);
        bool IsLevel2 = (index >= 5) && (index <= 7);
        bool IsLevel3 = (index >= 8) && (index <= 11);
        if (IsLevel1)
        {
            PlayLevel1Track();
        }
        else if (IsLevel2)
        {
            PlayLevel2Track();
        }
        else if (IsLevel3)
        {
            PlayLevel3Track();
        }
    }

    public void ToggleSFX()
    {
        switch (_CurrentSFXState)
        {
            case SFXState.ON:
                NotifyOnToggleOff?.Invoke();
                _CurrentSFXState = SFXState.OFF;
                break;
            case SFXState.OFF:
                NotifyOnToggleOn?.Invoke();
                _CurrentSFXState = SFXState.ON;
                break;
        }
    }

    private void OnEnable()
    {
        _LevelLoadManager.OnSwitchAudioTrack+=SoundSwitcher;
    }

    private void OnDisable()
    {
        _LevelLoadManager.OnSwitchAudioTrack-=SoundSwitcher;
    }

}
