﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXPlayer : MonoBehaviour
{
    [SerializeField]
    private RefSoundManager _SoundManager;
    [SerializeField]
    private AudioClip _ClipToPlay;
    private AudioSource _AudioSource;

    private void Awake()
    {
        _AudioSource = GetComponent<AudioSource>();
    }

    public void PlaySFX()
    {
        _AudioSource.clip = _ClipToPlay;
        _AudioSource.Play();
    }

    public void PlaySFX(AudioClip clip)
    {
        _AudioSource.clip = clip;
        _AudioSource.Play();
    }

    private void StopSFX()
    {
        _AudioSource.volume = 0.0f;
    }
    private void StartSFX()
    {
        _AudioSource.volume = 1.0f;
    }

    private void OnEnable()
    {
        if (_SoundManager.Value == null)
        {
            return;
        }
        _SoundManager.Value.NotifyOnToggleOff += StopSFX;
        _SoundManager.Value.NotifyOnToggleOn += StartSFX;
    }

    private void OnDisable()
    {
        if (_SoundManager.Value == null)
        {
            return;
        }
        _SoundManager.Value.NotifyOnToggleOff -= StopSFX;
        _SoundManager.Value.NotifyOnToggleOn -= StartSFX;
    }
}
