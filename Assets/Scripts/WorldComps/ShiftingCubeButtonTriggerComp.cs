﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftingCubeButtonTriggerComp : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        ShiftingCubeButtonPanelComp buttonPanel = other.gameObject.GetComponent<ShiftingCubeButtonPanelComp>();

        if (buttonPanel)
        {
            buttonPanel.TryToShiftCube();
        }
    }
}
