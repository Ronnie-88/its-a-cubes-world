﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWorkBlock : MonoBehaviour
{
   [SerializeField]
   private GameObject _FireWork;
   [SerializeField]
   private float _Duration;
   [SerializeField]
   private List<AudioClip> FireworkSfx;
   [SerializeField]
   private SFXPlayer _SFXPlayer;
   public bool StillActive{get; private set;}

   public void TriggerFireWorks()
   {
       if (StillActive)
       {
           return;
       }
       _FireWork.SetActive(true);
       _SFXPlayer.PlaySFX(GetRandomFireWorkSFX());
       Invoke("TurnOffFireWorks",_Duration);
        StillActive = true;
   }

   private void TurnOffFireWorks()
   {
       _FireWork.SetActive(false);
       StillActive = false;
   }

   private AudioClip GetRandomFireWorkSFX()
   {
       int index = Random.Range(0, FireworkSfx.Count);

       return FireworkSfx[index];
   }
}
