﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinGate : MonoBehaviour
{
    [SerializeField]
    private ObjectiveTrackingManager _ObjectiveTracker;
    [SerializeField]
    private CheckPointManager _CheckPointManager;
    [SerializeField]
    private RefUIBlackScreen _UIBlackScreen;
    [SerializeField]
    private Color _WinColour;
    [SerializeField]
    private WinGateColourSwitcher _WinGateColourSwitcher;

    private void Start()
    {
        Invoke("CheckToSwitchColor",0.5f);
    }



    private void OnTriggerEnter(Collider other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {

            if (_ObjectiveTracker.IsObjectiveReached())
            {
                _ObjectiveTracker.RemoveKeyCubes();
                // _ObjectiveTracker.RemoveLevelKeyCubeIndices();
                _CheckPointManager.ClearCheckpoints();
                _UIBlackScreen.Value.CrossOutFadeDefaultLoad();
                _CheckPointManager.CurrentCheckpointIndex = 0;
            }
        }
    }

    private void CheckToSwitchColor()
    {
        if (_ObjectiveTracker.IsObjectiveReached())
        {
            _WinGateColourSwitcher.SetWinColour(_WinColour);
        }
    }

    private void OnEnable()
    {
        _ObjectiveTracker.NotifyOnUpdateCurrentObjective += CheckToSwitchColor;
    }

    private void OnDisable()
    {
        _ObjectiveTracker.NotifyOnUpdateCurrentObjective -= CheckToSwitchColor;
    }

}
