﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPlatformCubeComp : MonoBehaviour, ICubeAction
{
    [SerializeField]
    private RefMainCameraData _MainCameraData;
    [SerializeField]
    private float _RotateSpeed;
    // private Vector3 _AxisOfRotation;
    private float _CurrentRotationAngle;


    private void OnCollisionEnter(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            player.transform.SetParent(this.transform);
            _MainCameraData.Value.SwitchCameraUpdateMethod();
        }
    }

    private void OnCollisionExit(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            player.transform.SetParent(null);
            _MainCameraData.Value.SwitchCameraUpdateMethod();
        }
    }

    private void Update()
    {
        TriggerCubeAction();
    }

    public void TriggerCubeAction()
    {
        RotatePlatforms();
    }

    private void RotatePlatforms()
    {
        _CurrentRotationAngle += (_RotateSpeed * Time.deltaTime);
        _CurrentRotationAngle = _CurrentRotationAngle % 360.0f;

        transform.rotation = Quaternion.Euler(transform.localEulerAngles.x, _CurrentRotationAngle, transform.localEulerAngles.z);
    }
}
