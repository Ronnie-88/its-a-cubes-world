﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pyrotechinques : MonoBehaviour
{
    [SerializeField]
    private List<FireWorkBlock> FireWorkBlocks;
    [SerializeField]
    private float _DelayDuration;

    private void Awake()
    {
        InvokeRepeating("TriggerSingleFireWorks", 0.2f,_DelayDuration);
    }

    private void TriggerSingleFireWorks()
    {
        int randomIndex = Random.Range(0, FireWorkBlocks.Count);
        if (FireWorkBlocks[randomIndex].StillActive)
        {
            randomIndex = Random.Range(0, FireWorkBlocks.Count);
            FireWorkBlocks[randomIndex].TriggerFireWorks();
            return;
        }
        FireWorkBlocks[randomIndex].TriggerFireWorks();
    }
}
