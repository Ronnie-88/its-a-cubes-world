﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCubeComp : MonoBehaviour, ICubeAction
{

    [SerializeField]
    private RefMainCameraData _MainCameraData;
    [SerializeField]
    private Transform _InitialPosition;
    [SerializeField]
    private Transform _TargetPosition;
    [SerializeField]
    private float _CubeMoveDuration;
    [SerializeField]
    private float _CubeMoveSpeed;
    private float _InterpTime;
    private float _CubeAlpha;


    private void Update()
    {
        CalculateAlpha();
        CalculateInterpTime();
        TriggerCubeAction();

    }


    private void OnCollisionEnter(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            player.transform.SetParent(this.transform);
            _MainCameraData.Value.SwitchCameraUpdateMethod();
        }
    }

    private void OnCollisionExit(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            player.transform.SetParent(null);
            _MainCameraData.Value.SwitchCameraUpdateMethod();
        }
    }



    private void CalculateInterpTime()
    {
        _InterpTime = _CubeAlpha / _CubeMoveDuration;
        //_InterpTime =  (_CubeMoveDuration * Mathf.Abs(Mathf.Cos(Time.time)))/_CubeMoveDuration;
        _InterpTime = _InterpTime * _InterpTime * (3.0f - 2.0f * _InterpTime);
    }

    public void TriggerCubeAction()
    {
        Vector3 resultPosition = Vector3.Lerp(_InitialPosition.position, _TargetPosition.position, _InterpTime);

        transform.position = resultPosition;
    }

    private void CalculateAlpha()
    {
        _CubeAlpha = Mathf.PingPong(Time.time * _CubeMoveSpeed, _CubeMoveDuration);
    }




}
