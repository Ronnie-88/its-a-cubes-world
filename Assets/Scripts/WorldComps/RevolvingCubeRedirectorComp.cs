﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RevolvingCubeRedirectorComp : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        RevolvingCubeComp revolvingCube = other.gameObject.GetComponent<RevolvingCubeComp>();
        
        if (revolvingCube != null)
        {
            revolvingCube.SecondRevolvingCubeComp.RotationDirection = -revolvingCube.SecondRevolvingCubeComp.RotationDirection;
            
        }
    }


}
