using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RawTimerNs;

public class RevolvingCubeComp : MonoBehaviour, ICubeAction
{
    [SerializeField]
    private CheckPointManager _CheckPointManger;
    [SerializeField]
    private RevolvingCubeComp _SecondRevolvingCubeComp;
    public RevolvingCubeComp SecondRevolvingCubeComp { get { return _SecondRevolvingCubeComp; } }
    [SerializeField]
    private float _MoveSpeed;
    [SerializeField]
    private RefMainCameraData _MainCameraData;
    [SerializeField]
    private MeshRenderer _MeshRenderer;
    [SerializeField, ColorUsage(true, true)]
    private Color _MeshPlayerOnColor;
    private Color _MeshDefaultColor;

    private bool _CanMoveToPos;
    public float RotationDirection { get; set; }

    private Vector3 _InitialPosition;

    private void Awake()
    {
        _MeshDefaultColor = _MeshRenderer.material.color;
        RotationDirection = -1.0f;
        _InitialPosition = transform.localPosition;
    }

    private void Update()
    {
        MoveCubeToNewPosition();
    }

    private void OnCollisionEnter(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            _MeshRenderer.material.color = _MeshPlayerOnColor;
            TriggerCubeAction();
            player.transform.SetParent(this.transform);
            _MainCameraData.Value.SwitchCameraUpdateMethod();
            RotationDirection = -RotationDirection;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            _MeshRenderer.material.color = _MeshDefaultColor;
            TriggerCubeAction();
            player.transform.SetParent(null);
            _MainCameraData.Value.SwitchCameraUpdateMethod();
        }
    }

    private void OnEnable()
    {
        //GameInstance.NotifyToResetToCheckpoint += ResetCubeActions;//old
        _CheckPointManger.NotifyToResetToCheckpoint += ResetCubeActions;//new
    }

    private void OnDisable()
    {
        //GameInstance.NotifyToResetToCheckpoint -= ResetCubeActions;//old
        _CheckPointManger.NotifyToResetToCheckpoint -= ResetCubeActions;//new
    }



    public void TriggerCubeAction()
    {
        _CanMoveToPos = !_CanMoveToPos;
    }

    private void ResetCubeActions()
    {
        transform.localPosition = _InitialPosition;
    }

    private void MoveCubeToNewPosition()
    {
        if (!_CanMoveToPos)
        {
            return;
        }
        _SecondRevolvingCubeComp.transform.RotateAround(transform.position, transform.up, (Time.deltaTime * _MoveSpeed * RotationDirection));
        _SecondRevolvingCubeComp.transform.rotation = Quaternion.identity;
    }
}
