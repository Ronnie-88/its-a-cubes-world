﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RawTimerNs;

public class FallingCubeComp : MonoBehaviour, ICubeAction
{
    [SerializeField]
    private CheckPointManager _CheckPointManger;
    [SerializeField]
    private SFXPlayer _SFXPlayerTicking;
    [SerializeField]
    private float _FallDuration;
    [SerializeField]
    private float _ColorDuration;
    [SerializeField, ColorUsage(true, true)]
    private Color _FallingColor;
    private Rigidbody _Rigidbody;
    private Material _CubeMaterial;
    private Color _DefaultCubeColour;
    private Collider _CubeCollider;
    private MeshRenderer _CubeMeshRenderer;
    private CubeTileColorSwitcher _CubeTileColorSwitcher;
    private AdditiveRawTimer _FallSequenceTimer = new AdditiveRawTimer();
    private SubtractiveRawTimer _DestroySequenceTimer = new SubtractiveRawTimer();
    private bool _TriggeredFall;
    private Vector3 _InitialPosition;
    private Color _NewColour;

    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody>();
        _CubeCollider = GetComponent<Collider>();
        _CubeMeshRenderer = GetComponentInChildren<MeshRenderer>();
        _CubeTileColorSwitcher = GetComponentInChildren<CubeTileColorSwitcher>();
        _Rigidbody.isKinematic = true;
        _DefaultCubeColour = _CubeTileColorSwitcher.GetMainColour();
        _InitialPosition = transform.position;
    }

    private void Update()
    {
        _FallSequenceTimer.TimerTick(Time.deltaTime);
        _DestroySequenceTimer.TimerTick(Time.deltaTime);
        // _CubeTileColorSwitcher.ChangeOutterCubeColour(_NewColour);

    }

    private void OnCollisionEnter(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            if (_TriggeredFall == true)
            {
                return;
            }
            _FallSequenceTimer.StartTimer(_ColorDuration);
            PlayTickingSFX();
        }
    }

    private void OnEnable()
    {

        _FallSequenceTimer.NotifyOnTimerStart += Set_TriggeredFall;
        _FallSequenceTimer.NotifyOnTick += TriggerCubeAction;
        _FallSequenceTimer.NotifyOnTimerEnd += TriggerFall;
        _DestroySequenceTimer.NotifyOnTimerEnd += TriggerCubeDestruction;
        _DestroySequenceTimer.NotifyOnOverride += TriggerResetCubeActions;
        //GameInstance.NotifyToResetToCheckpoint += ResetCubeActions;//old
        _CheckPointManger.NotifyToResetToCheckpoint += ResetCubeActions;//new

    }

    private void OnDisable()
    {
        _FallSequenceTimer.NotifyOnTimerStart -= Set_TriggeredFall;
        _FallSequenceTimer.NotifyOnTick -= TriggerCubeAction;
        _FallSequenceTimer.NotifyOnTimerEnd -= TriggerFall;
        _DestroySequenceTimer.NotifyOnTimerEnd -= TriggerCubeDestruction;
        _DestroySequenceTimer.NotifyOnOverride -= TriggerResetCubeActions;
        //GameInstance.NotifyToResetToCheckpoint -= ResetCubeActions;//old
        _CheckPointManger.NotifyToResetToCheckpoint += ResetCubeActions;//new
    }

    public void TriggerCubeAction()
    {
        _NewColour = Color.Lerp(_DefaultCubeColour, _FallingColor, (_FallSequenceTimer.CurrentTime) / _ColorDuration);
        _CubeTileColorSwitcher.ChangeOutterCubeColour(_NewColour);
    }

    private void Set_TriggeredFall()
    {
        _TriggeredFall = true;
        _DestroySequenceTimer.StartTimer(_FallDuration);
    }

    private void TriggerFall()
    {
        _Rigidbody.isKinematic = false;
    }

    private void TriggerCubeDestruction()
    {
        _CubeMeshRenderer.enabled = false;
        _Rigidbody.isKinematic = true;
        _CubeCollider.enabled = false;
    }

    private void ResetCubeActions()
    {
        _DestroySequenceTimer.OverrideTimer();

    }

    private void TriggerResetCubeActions()
    {
        transform.position = _InitialPosition;
        _Rigidbody.isKinematic = true;
        _TriggeredFall = false;
        _CubeTileColorSwitcher.ChangeOutterCubeColour(_DefaultCubeColour);
        _CubeMeshRenderer.enabled = true;
        _CubeCollider.enabled = true;
    }

    private void PlayTickingSFX()
    {
        _SFXPlayerTicking.PlaySFX();
    }

}
