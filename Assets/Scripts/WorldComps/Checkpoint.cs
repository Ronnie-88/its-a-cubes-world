﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [SerializeField]
    private CheckPointManager _CheckPointManager;
    [SerializeField]
    private int _MyCheckPointIndex;
    [SerializeField]
    private Transform _SpawnPoint;
    public int MyCheckPointIndex { get; private set; }
    public bool WasTriggered { get; private set; }

    private void Awake()
    {
        MyCheckPointIndex = _MyCheckPointIndex;
        Invoke("AddCheckPointInvoker", 0.35f);
    }

    private void AddCheckPointInvoker()
    {
        _CheckPointManager.AddCheckpoint(this);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (WasTriggered == true)
        {
            return;
        }

        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            Vector3 newPostion = _SpawnPoint.position;
            _CheckPointManager.PlayerPostion = newPostion;
            _CheckPointManager.CurrentCheckpointIndex = _MyCheckPointIndex;
            WasTriggered = true;
        }
    }

}
