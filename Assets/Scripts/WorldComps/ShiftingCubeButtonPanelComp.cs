﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftingCubeButtonPanelComp : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer _CubeMesh;
    [SerializeField]
    private ShiftingCubeComp _CubeToShift;
    [SerializeField, ColorUsage(true, true)]
    private Color _ShiftingColor;
    private Color _DefualtColor;
    private void Awake()
    {
        _DefualtColor = _CubeMesh.material.color;
    }

    private void OnCollisionEnter(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player)
        {   
            _CubeMesh.material.color = _ShiftingColor;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player)
        {   
            _CubeMesh.material.color = _DefualtColor;
        }
    }

    public void TryToShiftCube()
    {
        _CubeToShift.TriggerCubeAction();
    }

}
