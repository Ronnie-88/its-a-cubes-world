﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCube : MonoBehaviour, ICubeAction
{
    // [SerializeField]
    // private int _MyKeyCubeIndex;
    // [SerializeField]
    // private RefBool _LoadOnce;
    [SerializeField]
    private ObjectiveTrackingManager _ObjectiveTracker;
    [SerializeField, ColorUsage(true, true)]
    private Color _ObjectiveCompleteColor;
    [SerializeField]
    private GameObject _ParticleFeedBack;
    private bool _WasKeyCubeTriggered;
    private MeshRenderer _CubeMeshRenderer;
    private Material _CubeMaterial;
    private CubeTileColorSwitcher _CubeTileColorSwitcher;
    private SFXPlayer _SFXPlayer;

    private void Awake()
    {
        _SFXPlayer = GetComponentInChildren<SFXPlayer>();
        _CubeTileColorSwitcher = GetComponentInChildren<CubeTileColorSwitcher>();
        // OnGameLoad();
        
    }

    private void Start()
    {
        _ObjectiveTracker.AddKeyCube(this);
    }


    private void OnCollisionEnter(Collision other)
    {
        if (_WasKeyCubeTriggered)
        { return; }

        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            // _ObjectiveTracker.AddKeyCubeTriggeredIndex(_MyKeyCubeIndex);
            _ObjectiveTracker.UpdateCurrentObjectiveInvoker();
            TriggerCubeAction();
        }
    }


    public void TriggerCubeAction()
    {
        _CubeTileColorSwitcher.ChangeOutterCubeColour(_ObjectiveCompleteColor);
        _SFXPlayer.PlaySFX();
        _ParticleFeedBack.SetActive(true);
        _WasKeyCubeTriggered = true;
    }

    // private void OnGameLoad()
    // {
    //     if (_LoadOnce.Value)
    //     {
    //         if (_ObjectiveTracker.GetKeyIndexAt(_MyKeyCubeIndex))
    //         {
    //             _WasKeyCubeTriggered = true;
    //             _CubeTileColorSwitcher.ChangeOutterCubeColour(_ObjectiveCompleteColor);
    //         }

    //     }
    // }

    private void OnDisable()
    {
    }

}
