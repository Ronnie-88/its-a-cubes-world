using UnityEngine;

[ExecuteInEditMode]
public class WinGateColourSwitcher : MonoBehaviour
{
    [SerializeField]
    private Color _MainColor;
    [SerializeField, Range(0.0f, 1.0f)]
    private float _Transparency;
    private MeshRenderer _MeshRenderer;
    private MaterialPropertyBlock _CurrentPropertyBlock;

    private void Awake()
    {
        _MeshRenderer = GetComponent<MeshRenderer>();
    }

    private void OnEnable()
    {
        SetMaterialInitialProperties();
    }

#if UNITY_EDITOR
    private void Update()
    {
        // if (Application.isPlaying == false)
        // {
        //     SetMaterialInitialProperties();
        // }
    }
#endif

    private void SetMaterialInitialProperties()
    {
        MaterialPropertyBlock newPropertyBlock = new MaterialPropertyBlock();
        newPropertyBlock.SetColor("_Color", _MainColor);
        newPropertyBlock.SetFloat("_Transparency", _Transparency);
        _MeshRenderer.SetPropertyBlock(newPropertyBlock);
        _CurrentPropertyBlock = newPropertyBlock;
    }

    public void SetWinColour(Color newColor)
    {
        MaterialPropertyBlock newPropertyBlock = _CurrentPropertyBlock;
        newPropertyBlock.SetColor("_Color", newColor);
        _MeshRenderer.SetPropertyBlock(newPropertyBlock);
        _CurrentPropertyBlock = newPropertyBlock;
    }

    public Color GetMainColour()
    {
        return _MainColor;
    }
}
