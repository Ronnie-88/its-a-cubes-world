﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathHandler : MonoBehaviour
{
    // [SerializeField]
    // private RefUIBlackScreen _UIBlackScreen;
    public static event Action NotifyOnPlayerDeath;

    private void OnTriggerEnter(Collider other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player!= null)
        {
            NotifyOnPlayerDeath?.Invoke();
        }
    } 
}
