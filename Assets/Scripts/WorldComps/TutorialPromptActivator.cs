﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPromptActivator : MonoBehaviour
{
    [SerializeField]
    private RefUIManager _UIManager;
    [SerializeField]
    private int TutorialIndex;

    private void OnTriggerEnter(Collider other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            _UIManager.Value.TutorialPromptShower(TutorialIndex);
        }
    }

    private void OnDisable()
    {

    }
}
