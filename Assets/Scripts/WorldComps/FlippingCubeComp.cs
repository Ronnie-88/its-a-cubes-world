﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlippingCubeComp : MonoBehaviour, ICubeAction
{
    [SerializeField]
    private float _FlipSpeed;
    [SerializeField]
    private float _FlipAngleMax;
    [SerializeField]
    private float _FlipAngleMin;
    private bool _CanFlipForward;
    private bool _CanFlipBack;
    private Quaternion _CurrentRotation;
    private float _CurrentPosition;
    private Quaternion _CurrentRot;
    private SFXPlayer _SFXPlayer;
    private enum CubeFlip
    {
        FlipForward, FlipBack
    }
    private Rigidbody _Rigidbody;
    private CubeFlip _CurrentState;
    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody>();
        _SFXPlayer = GetComponentInChildren<SFXPlayer>();
        _CurrentState = CubeFlip.FlipBack;
    }

    private void Update()
    {


    }

    private void FixedUpdate()
    {
        FlipCubeToForward();
        FlipCubeToBack();

    }

    private void OnEnable()
    {
        PlayerInput.NotifyOnJumpConfirmed += TriggerCubeAction;
    }
    private void OnDisable()
    {
        PlayerInput.NotifyOnJumpConfirmed -= TriggerCubeAction;
    }

    public void TriggerCubeAction()
    {
        PlayMoveSFX();
        switch (_CurrentState)
        {
            case CubeFlip.FlipBack:
                _CurrentState = CubeFlip.FlipForward;
                _CanFlipBack = true;
                _CanFlipForward = false;
                break;
            case CubeFlip.FlipForward:
                _CurrentState = CubeFlip.FlipBack;
                _CanFlipForward = true;
                _CanFlipBack = false;
                break;
        }
    }

    private void FlipCubeToBack()
    {
        if (!_CanFlipBack)
        { return; }
        _CurrentPosition -= (Time.deltaTime * _FlipSpeed);
        _CurrentPosition = Mathf.Clamp(_CurrentPosition, _FlipAngleMin, _FlipAngleMax);
        _CurrentRot = Quaternion.Euler(_Rigidbody.transform.localEulerAngles.x, _Rigidbody.transform.localEulerAngles.y, _CurrentPosition);
      
        _Rigidbody.MoveRotation(_CurrentRot);

        if (_CurrentPosition >= _FlipAngleMax)
        {
            _CanFlipBack = false;
        }
    }

    private void FlipCubeToForward()
    {
        if (!_CanFlipForward)
        { return; }
        _CurrentPosition += (Time.deltaTime * _FlipSpeed);
        _CurrentPosition = Mathf.Clamp(_CurrentPosition, _FlipAngleMin, _FlipAngleMax);
        _CurrentRot = Quaternion.Euler(_Rigidbody.transform.localEulerAngles.x, _Rigidbody.transform.localEulerAngles.y, _CurrentPosition);
        _Rigidbody.MoveRotation(_CurrentRot);

        if (_CurrentPosition <= _FlipAngleMin)
        {
            _CanFlipForward = false;
        }
    }

    private void PlayMoveSFX()
    {
        _SFXPlayer.PlaySFX();
    }
}
