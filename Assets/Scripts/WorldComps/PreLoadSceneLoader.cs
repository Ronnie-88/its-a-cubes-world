using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreLoadSceneLoader : MonoBehaviour
{
    [SerializeField]
    private LevelLoadManager _LoadLevelManager;

    private void Start()
    {
        _LoadLevelManager.LoadNewScene();
    }
}
