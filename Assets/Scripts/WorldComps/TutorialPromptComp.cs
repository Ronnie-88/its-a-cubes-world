﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPromptComp : MonoBehaviour
{
    [SerializeField]
    private int _MyIndex;
    public int MyIndex{get{return _MyIndex;}}
    
    public void RemovePrompt()
    {
        this.gameObject.SetActive(false);
    }

    public void ShowPrompt()
    {
        this.gameObject.SetActive(true); 
    }
}
