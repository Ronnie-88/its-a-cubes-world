﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class EndGameCameraComp : MonoBehaviour
{
    [SerializeField]
    private CinemachineVirtualCamera _VirtualCamera;
    [SerializeField]
    private float _MoveSpeed;
    private float _CurrentSpeed;
    private CinemachineTrackedDolly _CinemachineTrackedDolly;

    private void Awake()
    {
        _CinemachineTrackedDolly = _VirtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>();
    }

    private void Update()
    {
        MoveCameraThroughPoint();
    }

    private void MoveCameraThroughPoint()
    {
        _CurrentSpeed +=(Time.deltaTime*_MoveSpeed);
        _CurrentSpeed = _CurrentSpeed %  5.0f;

        _CinemachineTrackedDolly.m_PathPosition = _CurrentSpeed;
    }
}
