﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftingCubeComp : MonoBehaviour, ICubeAction
{
    [SerializeField]
    private CheckPointManager _CheckPointManger;
    [SerializeField, ColorUsage(true, true)]
    private Color _ShiftingColor;
    [SerializeField]
    private RefMainCameraData _MainCameraData;
    [SerializeField]
    private float _ShiftSpeed;
    [SerializeField]
    private float _MaxDistance;
    private float _CurrentShiftPosition;
    private float _ShiftDirection;//(1= frwd, 0= stop -1= back)
    private bool _CanShift;
    private bool _CanShiftColour;
    private Vector3 _InitialPosition;
    private Color _DefaultColor;
    private MeshRenderer _CubeMesh;

    private enum ShiftState { SHIFTFORWARD, SHIFTBACKWARD, NOSHIFT }
    private ShiftState _CurrentShiftState;


    private void Awake()
    {
        _CubeMesh = GetComponentInChildren<MeshRenderer>();
        _InitialPosition = transform.position;
        _CurrentShiftState = ShiftState.SHIFTFORWARD;
        _DefaultColor = _CubeMesh.material.color;
    }

    private void FixedUpdate()
    {
        ShiftCube();
    }

    private void OnCollisionEnter(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            player.transform.SetParent(this.transform);
            _MainCameraData.Value.SwitchCameraUpdateMethod();
        }
    }

    private void OnCollisionExit(Collision other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null)
        {
            player.transform.SetParent(null);
            _MainCameraData.Value.SwitchCameraUpdateMethod();
        }
    }

    private void OnEnable()
    {
        _CheckPointManger.NotifyToResetToCheckpoint += ResetCubeActions;
    }

    private void OnDisable()
    {
        _CheckPointManger.NotifyToResetToCheckpoint -= ResetCubeActions;
    }

    public void TriggerCubeAction()
    {
        SwitchShiftState();
    }

    public void SwitchShiftState()
    {
        switch (_CurrentShiftState)
        {
            case ShiftState.SHIFTFORWARD:
                _CanShift = true;
                _CanShiftColour = true;
                _ShiftDirection = 1.0f;
                _CurrentShiftState = ShiftState.NOSHIFT;
                break;
            case ShiftState.NOSHIFT:
                if (_ShiftDirection == 1.0f)
                {
                    _CurrentShiftState = ShiftState.SHIFTBACKWARD;
                }
                else
                {
                    _CurrentShiftState = ShiftState.SHIFTFORWARD;
                }
                _CanShift = false;
                _CanShiftColour = false;
                SwitchToDefaultColor();
                break;
            case ShiftState.SHIFTBACKWARD:
                _CanShift = true;
                _CanShiftColour = true;
                _ShiftDirection = -1.0f;
                _CurrentShiftState = ShiftState.NOSHIFT;
                break;

        }
    }

    private void ShiftCube()
    {
        if (!_CanShift) { return; }

        _CurrentShiftPosition += (_ShiftSpeed * _ShiftDirection * Time.deltaTime);
        _CurrentShiftPosition = Mathf.Clamp(_CurrentShiftPosition, 0.0f, _MaxDistance);

        if ((_CurrentShiftPosition >= _MaxDistance) || (_CurrentShiftPosition <= 0.0f))
        {
            CheckShiftAtEndPositions();
            _CanShift = false;
        }
        SwitchToShiftColor();
        transform.position = _InitialPosition + (transform.right * _CurrentShiftPosition);
    }

    private void ResetCubeActions()
    {
        _CanShift = false;
        SwitchToDefaultColor();
        _CurrentShiftPosition = 0.0f;
        _CurrentShiftState = ShiftState.SHIFTFORWARD;
        transform.position = _InitialPosition;
    }

    private void CheckShiftAtEndPositions()
    {
        if (_CurrentShiftPosition == _MaxDistance)
        {
            _CurrentShiftState = ShiftState.SHIFTBACKWARD;
            SwitchToDefaultColor();
        }

        if (_CurrentShiftPosition == 0.0f)
        {
            _CurrentShiftState = ShiftState.SHIFTFORWARD;
            SwitchToDefaultColor();
        }
    }

    private void SwitchToShiftColor()
    {
        if (_CanShiftColour)
        {
            _CubeMesh.material.color = _ShiftingColor;
            _CanShiftColour = false;
        }

    }
    private void SwitchToDefaultColor()
    {
        _CubeMesh.material.color = _DefaultColor;
    }
}
