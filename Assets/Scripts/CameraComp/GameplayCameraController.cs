using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GameplayCameraController : MonoBehaviour
{
    [SerializeField]
    private RefPlayerInput _PlayerInput;
    [SerializeField]
    private RefGameplayCameraController _GameplayCameraController;
    [SerializeField]
    private float _CameraShiftSpeed;
    [SerializeField]
    private RefMainCameraData _MainCamera;
    private float _CameraShiftMagnitude = 90.0f;
    private float _CurrentCameraAngle;
    private static float _MaxCameraAngle = 360.0f;
    private CinemachineVirtualCamera _VirtualCamera;
    private CinemachineOrbitalTransposer _OrbitalTransposer;
    public Camera MainCam { get; private set; }

    private void Awake()
    {
        //CinemachineCore.GetInputAxis = GetInputAxis;
        _VirtualCamera = GetComponent<CinemachineVirtualCamera>();
        _OrbitalTransposer = _VirtualCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>();    
        _GameplayCameraController.Value = GetComponent<GameplayCameraController>();
        
    }

    private void Start()
    {
        MainCam = _MainCamera.Value._MainCam;
    }

    private void OnEnable()
    {
        _PlayerInput.Value.OnLookPressed += GetNewCameraAngle;
    }

    private void OnDisable()
    {
        _PlayerInput.Value.OnLookPressed -= GetNewCameraAngle;
    }



    private float GetInputAxis(string axisName)
    {
        if (axisName == "Look X")
        {
            return _CurrentCameraAngle;
        }

        return 0.0f;
    }

    private void Update()
    {
        TransitionCamera();
    }

    private void TransitionCamera()
    {

        if (_OrbitalTransposer.m_XAxis.Value == 360.0f || _OrbitalTransposer.m_XAxis.Value == -360.0f)
        {
            _OrbitalTransposer.m_XAxis.Value = 0.0f;
            _CurrentCameraAngle = 0.0f;
        }
        else
        {
            _OrbitalTransposer.m_XAxis.Value = Mathf.MoveTowards(_OrbitalTransposer.m_XAxis.Value,
             _CurrentCameraAngle, Time.deltaTime * _CameraShiftSpeed);
        }
    }

    private void GetNewCameraAngle(float shiftMagnitude)
    {
        if (_OrbitalTransposer.m_XAxis.Value != _CurrentCameraAngle)
        {
            return;
        }
        float cameraShiftResult = shiftMagnitude * _CameraShiftMagnitude;

        float first = (_CurrentCameraAngle += cameraShiftResult); 

        if (  first == 360.0f || first == -360.0f)
        {
            _CurrentCameraAngle = shiftMagnitude * _MaxCameraAngle;
        }
        else
        {
            _CurrentCameraAngle = first;
            _CurrentCameraAngle %= _MaxCameraAngle;
        }
    }
}
