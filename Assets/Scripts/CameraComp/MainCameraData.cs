﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;

public class MainCameraData : MonoBehaviour
{
    [SerializeField]
    private RefMainCameraData _MainCameraData;
    [SerializeField]
    private LevelLoadManager _LevelLoadManager;
    private CinemachineBrain _CinemachineBrain;
    private CinemachineBrain.UpdateMethod _CameraUpdateMethod;
    public Camera _MainCam {get; private set;}
    private void Awake()
    {
        _MainCam = GetComponent<Camera>();
        _CinemachineBrain = GetComponent<CinemachineBrain>();
        _MainCameraData.Value = GetComponent<MainCameraData>();
        _CameraUpdateMethod = CinemachineBrain.UpdateMethod.FixedUpdate;
        _CinemachineBrain.m_UpdateMethod = _CameraUpdateMethod;
    }

    public void SwitchCameraUpdateMethod()
    {
        switch (_CameraUpdateMethod)
        {
            case CinemachineBrain.UpdateMethod.FixedUpdate:
                _CameraUpdateMethod = CinemachineBrain.UpdateMethod.LateUpdate;
                _CinemachineBrain.m_UpdateMethod = _CameraUpdateMethod;
                break;
            case CinemachineBrain.UpdateMethod.LateUpdate:
                _CameraUpdateMethod = CinemachineBrain.UpdateMethod.FixedUpdate;
                _CinemachineBrain.m_UpdateMethod = _CameraUpdateMethod;
                break;

        }
    }

    private void ResetPosition()
    {
        transform.position = Vector3.zero;
        transform.rotation = Quaternion.identity;
    }

    private void OnEnable()
    {
        Invoke("ResetPosition",0.25f);
    }


    private void OnDisable()
    {
    }





}
