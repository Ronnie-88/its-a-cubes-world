﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityComponent : MonoBehaviour
{
    [SerializeField, Range(0.0f, 100.0f)]
    private float _GravityScale = 1.0f;
    private const float GLOBAL_GRAVITY = -9.81f;
    private float _CurrentGravity;
    private Rigidbody _Rigidbody;


    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        _CurrentGravity = GLOBAL_GRAVITY * _GravityScale;

        UseGravity(transform.up, _CurrentGravity);
    }

    private void UseGravity(Vector3 direction, float scale)
    {
        _Rigidbody.AddForce(direction * scale, ForceMode.Acceleration);
    }
}
