﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInput : MonoBehaviour
{
    [SerializeField]
    private RefPlayerInput _PlayerInput;
    public Vector2 MoveDirection { get; private set; }
    public float CameraLookAxis { get; private set; }
    public event Action OnJumpPressed;
    public static event Action NotifyOnJumpConfirmed;
    public static event Action NotifyEnablePlayerInput;
    public static event Action NotifyDisablePlayerInput;
    public event Action<float> OnLookPressed;

    private CubeInput _CubeInputAsset;

    private void Awake()
    {
        _CubeInputAsset = new CubeInput();
        _PlayerInput.Value = GetComponent<PlayerInput>();
    }

    private void OnEnable()
    {
        // NotifyEnablePlayerInput += EnablePlyerInput;
        // NotifyDisablePlayerInput += DisablePlayerInput;
        EnablePlyerInput();
        SubcribeFunctions();
    }

    private void OnDisable()
    {

        // NotifyEnablePlayerInput -= EnablePlyerInput;
        // NotifyDisablePlayerInput -= DisablePlayerInput;
        DisablePlayerInput();
        UnSubcribeFunctions();
    }

    private void SubcribeFunctions()
    {
        _CubeInputAsset.Gameplay.Move.performed += OnMovePerformed;
        _CubeInputAsset.Gameplay.Move.canceled += OnMoveCanceled;
        _CubeInputAsset.Gameplay.Jump.performed += OnJumpPerformed;
        _CubeInputAsset.Gameplay.Look.performed += OnLookPerformed;
    }

    private void UnSubcribeFunctions()
    {
        _CubeInputAsset.Gameplay.Move.performed -= OnMovePerformed;
        _CubeInputAsset.Gameplay.Move.canceled -= OnMoveCanceled;
        _CubeInputAsset.Gameplay.Jump.performed -= OnJumpPerformed;
        _CubeInputAsset.Gameplay.Look.performed -= OnLookPerformed;
    }

    private void OnMovePerformed(InputAction.CallbackContext context)
    {
        MoveDirection = context.ReadValue<Vector2>();
    }

    private void OnJumpPerformed(InputAction.CallbackContext context)
    {
        OnJumpPressed?.Invoke();
    }

    public void ConfirmJumpPressed()
    {
        NotifyOnJumpConfirmed?.Invoke();
    }

    private void OnMoveCanceled(InputAction.CallbackContext context)
    {
        MoveDirection = Vector2.zero;
    }
    private void OnLookPerformed(InputAction.CallbackContext context)
    {
        OnLookPressed?.Invoke(context.ReadValue<float>());
    }

    public static void InvokeEnable()
    {
        NotifyEnablePlayerInput?.Invoke();
    }
    public static void InvokeDisable()
    {
        NotifyDisablePlayerInput?.Invoke();
    }

    public void DisablePlayerInput()
    {
        _CubeInputAsset.Gameplay.Disable();
    }

    public void EnablePlyerInput()
    {
        _CubeInputAsset.Gameplay.Enable();
    }

}
