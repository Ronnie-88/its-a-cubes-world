using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerSaveProfile
{
    //this class is reponsible for delegating all saved and loaded data to the game
    //we save only when we quit or reach a checkpoint or reload player
    private static PlayerSaveProfile _Instance;

    public int CurrentCheckpointIndex { get; set; }
    public int CurrentLevelIndex { get; set; }
    public float LevelKeyCountMax { get; set; }
    public float CurrentLevelKeyCubesReached { get; set; }
    private Float3 _LastPlayerPostion;
    public Float3 LastPlayerPostion { get => _LastPlayerPostion;}
    private List<int> _KeyCubesTriggeredIndices; 
    public List<int> KeyCubesTriggeredIndices { get => _KeyCubesTriggeredIndices;}
    private List<int> _CheckPointTriggeredIndices; 
    public List<int> CheckPointTriggeredIndices { get => _CheckPointTriggeredIndices;}
    
    public static PlayerSaveProfile Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = new PlayerSaveProfile();
            }
            return _Instance;
        }
        set { _Instance = value; }
    }


    public void SaveTriggeredKeyCubeIndex(List<int> KeyCubesTriggered)
    {
        _KeyCubesTriggeredIndices = KeyCubesTriggered;
    }
    public void SaveTriggeredCheckpointIndex(List<int> checkPointsTriggered)
    {
        _CheckPointTriggeredIndices = checkPointsTriggered;
    }

    public void SetNewPlayerPosition(Float3 float3)
    {
        _LastPlayerPostion = float3;
    }

}


