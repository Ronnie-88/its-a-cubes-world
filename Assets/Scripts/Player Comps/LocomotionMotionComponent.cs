﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocomotionMotionComponent : IMove<Rigidbody>
{

    public void AddMotion(Rigidbody motionComponent, Vector3 direction, float motionMagnitude)
    {
        motionComponent.AddForce(direction * motionMagnitude, ForceMode.VelocityChange);    
    }
}
