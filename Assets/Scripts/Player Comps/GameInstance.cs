﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInstance : MonoBehaviour
{
    //A game instance represents represents a running instance of the game when destroyed all parameters not saved will be reset

    [SerializeField]
    private LevelLoadManager _LevelLoadManager;
    [SerializeField]
    private CheckPointManager _CheckPointManager;
    [SerializeField]
    private ObjectiveTrackingManager _ObjectiveTracker;
    // [SerializeField]
    // private RefBool _LoadOnce;
    [SerializeField]
    private RefGameInstance _GameInstance;
    private string _SaveFilePath;

    private void Awake()
    {
        ClearTrackers();
        _GameInstance.Value = this;
        _SaveFilePath = Application.persistentDataPath + "/GameSaves/IWCSaveFile.sd";
    }

    public void SwitchToSavedLevel()
    {
        if (!DataSerializer.DoesSaveFileExists())
        { return; }
        ClearTrackers();
        // _LoadOnce.Value = true;
        PlayerSaveProfile.Instance = (PlayerSaveProfile)DataSerializer.LoadGameData(_SaveFilePath);
        _LevelLoadManager.LoadNewScene(PlayerSaveProfile.Instance.CurrentLevelIndex);
    }

    public void ClearTrackers()
    {
        _ObjectiveTracker.RemoveKeyCubes();
        _CheckPointManager.ClearCheckpoints();
    }

    public void SaveGameInstance()
    {
        _LevelLoadManager.SaveToPlayerProfile();
        DataSerializer.SaveGameData("IWCSaveFile", PlayerSaveProfile.Instance);
    }

    private void OnApplicationQuit()
    {
        // _LoadOnce.Value = false;
    }

}
