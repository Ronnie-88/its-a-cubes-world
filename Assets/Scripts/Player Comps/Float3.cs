using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Float3
{
    float x, y, z;

    public float X { get => x; set => x = value; }
    public float Y { get => y; set => y = value; }
    public float Z { get => z; set => z = value; }

    public Float3()
    {
        this.x = 0.0f;
        this.y = 0.0f;
        this.z = 0.0f;
    }
    public Float3(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
