﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFeedBackComp : MonoBehaviour
{

    [SerializeField]
    private float _ParticleDuration;
    [SerializeField]
    private GameObject _LandParticle;
    [SerializeField]
    private SFXPlayer _LandSFXPlayer;
    private SFXPlayer _SFXPlayer;
    private void Awake()
    {
        _SFXPlayer = GetComponentInChildren<SFXPlayer>();
    }

    private void OnCollisionEnter(Collision other)
    {
        ToggleJumpLandParticle();
        PlayLandSFX();
    }

    private void ToggleJumpLandParticle()
    {
        _LandParticle.SetActive(true);
        Invoke("TurnOffParticle", _ParticleDuration);
    }

    private void TurnOffParticle()
    {
        _LandParticle.gameObject.SetActive(false);
    }

    private void PlayLandSFX()
    {
        _LandSFXPlayer.PlaySFX();
    }



    private void OnEnable()
    {
        PlayerInput.NotifyOnJumpConfirmed += _SFXPlayer.PlaySFX;
    }

    private void OnDisable()
    {
        PlayerInput.NotifyOnJumpConfirmed -= _SFXPlayer.PlaySFX;
    }
}
