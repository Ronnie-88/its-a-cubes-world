﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PlayerInput))]
public class PlayerActionInterface : MonoBehaviour
{
    [SerializeField]
    private RefGameplayCameraController _GameplayCameraController;
    [SerializeField]
    private float _MoveSpeed;
    [SerializeField]
    private float _TurnDelta;
    [SerializeField]
    private float _JumpForce;
    [SerializeField]
    private int _NumberOfJumps;
    private Rigidbody _Rigidbody;
    private PlayerInput _PlayerInput;
    private Vector3 _PlayerTargetMoveDirection;
    private LocomotionMotionComponent _HorizontalMotion = new LocomotionMotionComponent();
    private LocomotionMotionComponent _JumpMotion = new LocomotionMotionComponent();
    private GroundDetectorComponent _GroundDetector;
    private int _CurrentNumberOfJumps;
    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody>();
        _PlayerInput = GetComponent<PlayerInput>();
        _GroundDetector = GetComponent<GroundDetectorComponent>();
        _CurrentNumberOfJumps = _NumberOfJumps;
    }

    private void Update()
    {
        ApplyTargetDirection();
        ResetJump();
    }

    private void FixedUpdate()
    {
        MovePlayer();
    }

    private void OnEnable()
    {
        _PlayerInput.OnJumpPressed += JumpPlayer;
    }

    private void OnDisable()
    {
        _PlayerInput.OnJumpPressed -= JumpPlayer;
    }


    private void MovePlayer()
    {
        _PlayerTargetMoveDirection.Normalize();
        _HorizontalMotion.AddMotion(_Rigidbody, _PlayerTargetMoveDirection, _MoveSpeed);
        CancelHorizontalVelocity();
    }

    private void JumpPlayer()
    {
        if (_GroundDetector.IsGrounded && (_CurrentNumberOfJumps > 0))
        {
            _JumpMotion.AddMotion(_Rigidbody, Vector3.down, _Rigidbody.velocity.y);
            _JumpMotion.AddMotion(_Rigidbody, transform.up, _JumpForce);
            _PlayerInput.ConfirmJumpPressed();
            _CurrentNumberOfJumps--;
        }

    }

    private void ResetJump()
    {
        if (_GroundDetector.IsGrounded == false)
        {
            return;
        }
        _CurrentNumberOfJumps = _NumberOfJumps;
    }

    private void GetTargetDirection()
    {
        _PlayerInput.MoveDirection.Normalize();
        Vector3 camForward = _GameplayCameraController.Value.MainCam.transform.forward;
        Vector3 camRight = _GameplayCameraController.Value.MainCam.transform.right;
        camForward.y = 0.0f;
        camRight.y = 0.0f;

        _PlayerTargetMoveDirection = (camForward * _PlayerInput.MoveDirection.y) + (camRight * _PlayerInput.MoveDirection.x);


        Quaternion lookDirection = Quaternion.LookRotation(_PlayerTargetMoveDirection);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookDirection, _TurnDelta * Time.deltaTime);
    }

    private void ApplyTargetDirection()
    {
        if (_PlayerInput.MoveDirection.sqrMagnitude != 0.0f)
        {
            GetTargetDirection();
        }
        else
        {
            _PlayerTargetMoveDirection = Vector3.zero;
        }
    }

    private void CancelHorizontalVelocity()
    {
        _HorizontalMotion.AddMotion(_Rigidbody, Vector3.forward, -_Rigidbody.velocity.z);
        _HorizontalMotion.AddMotion(_Rigidbody, Vector3.right, -_Rigidbody.velocity.x);
    }

}
