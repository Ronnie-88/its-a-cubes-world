﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetectorComponent : MonoBehaviour
{
    [SerializeField]
    private LayerMask _LayerMask;
    [SerializeField]
    private float _MaxDistance;
    [SerializeField]
    private Vector3 _HalfExtents;
    public bool IsGrounded { get; private set; }

    private void Update()
    {
        IsGrounded = RayHit();
    }

    public bool RayHit()
    {
        return Physics.BoxCast(transform.position, _HalfExtents, -transform.up, Quaternion.identity, _MaxDistance, _LayerMask);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        //Check if there has been a hit yet
        if (IsGrounded)
        {
            Gizmos.color = Color.green;
            //Draw a Ray forward from GameObject toward the hit
            Gizmos.DrawRay(transform.position, -transform.up * _MaxDistance);
            //Draw a cube that extends to where the hit exists
            Gizmos.DrawWireCube(transform.position + (-transform.up * _MaxDistance), _HalfExtents);
        }
        //If there hasn't been a hit yet, draw the ray at the maximum distance
        else
        {
            Gizmos.color = Color.red;
            //Draw a Ray forward from GameObject toward the maximum distance
            Gizmos.DrawRay(transform.position, -transform.up * _MaxDistance);
            //Draw a cube at the maximum distance
             Gizmos.DrawWireCube(transform.position + (-transform.up * _MaxDistance), _HalfExtents);
        }
    }
}
