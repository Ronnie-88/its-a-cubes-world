using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public class DataSerializer 
{
    public static void SaveGameData(string saveName, object saveData)
    {
        string pathToSaveFile = "";
        string pathToDirectory = Application.persistentDataPath + "/GameSaves";
        string fileType = ".sd";
        BinaryFormatter formatter = RequestFormatter();
        Directory.CreateDirectory(pathToDirectory);
        pathToSaveFile = pathToDirectory+"/"+saveName+fileType;
        if (File.Exists(pathToSaveFile))
        {
            File.Delete(pathToSaveFile);
        }

        FileStream file = File.Create(pathToSaveFile);
        formatter.Serialize(file,saveData);
        file.Close();
    }

    public static bool DoesSaveFileExists()
    {
        string filePath = Application.persistentDataPath +"/GameSaves/IWCSaveFile.sd";
        return File.Exists(filePath);
    }

    public static void OverWriteSaveFileOnNewGame()
    {
         string pathToDirectory = Application.persistentDataPath + "/GameSaves";
         if (!Directory.Exists(pathToDirectory))
         {
             return;
         }
         string[] files = Directory.GetFiles(pathToDirectory);
         foreach (string item in files)
         {
             File.Delete(item);
         }
    }

    public static object LoadGameData(string filePath)
    {
        if (!File.Exists(filePath))
        {
            return null;
        }
        BinaryFormatter formatter =  RequestFormatter();
        FileStream saveFile = File.Open(filePath, FileMode.Open);
        
        try
        {
            object saveData = formatter.Deserialize(saveFile);
            saveFile.Close();
            return saveData;
        }
        catch (System.Exception)
        {
            Debug.LogErrorFormat("Failed to load file: {0}",filePath);
            throw new FileNotFoundException();
        }
        
    }

    private static BinaryFormatter RequestFormatter()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        return formatter;
    }
}
