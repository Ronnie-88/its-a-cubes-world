﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private ObjectiveTrackingManager _ObjectiveTracker;
    [SerializeField]
    private RefGameInstance _GameInstance;
    [SerializeField]
    private CheckPointManager _CheckPointManager;
    [SerializeField]
    private LevelLoadManager _LevelLoadManager;
    [SerializeField]
    private RefPlayer _PlayerRef;
    [SerializeField]
    private RefBool _LoadOnce;
    [SerializeField]
    private RefObjectiveProgressBar _ObjectiveProgressBar;
    private Rigidbody _Rigidbody;
    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody>();
        _PlayerRef.Value = GetComponent<Player>();

    }

    private void Start()
    {
        Invoke("InitializeCurrentObjectiveInvoker", 0.35f);
        _CheckPointManager.GetNewStartingPosition(transform.position);
        _ObjectiveProgressBar.Value.UpdateBarOnStart();
    }

    private void InitializeCurrentObjectiveInvoker()
    {
        _ObjectiveTracker.InitializeCurrentObjective();
        _GameInstance.Value.SaveGameInstance();
    }

    // private void LoadOnceInvoker()
    // {
    //     _LoadOnce.Value = false;
    // }

    private void ResetPositionOnDeath()
    {
        MoveToPoint(_CheckPointManager.PlayerPostion);
    }

    private void ResetPostionOnLoadGame()
    {
        MoveToPoint(_CheckPointManager.PlayerPostion);
    }

    private void MoveToPoint(Vector3 point)
    {
        _Rigidbody.isKinematic = true;
        _Rigidbody.transform.position = point;
        transform.rotation = Quaternion.identity;
        _Rigidbody.isKinematic = false;
    }


    // private void OnLoadGame()
    // {
    //     ResetPostionOnLoadGame();
    // }


    private void OnEnable()
    {
        _CheckPointManager.NotifyToResetToCheckpoint += ResetPositionOnDeath;

    }

    private void OnDisable()
    {
        _CheckPointManager.ClearCheckpoints();
        _CheckPointManager.NotifyToResetToCheckpoint -= ResetPositionOnDeath;
    }
}
