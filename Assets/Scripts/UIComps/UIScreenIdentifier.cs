﻿using UnityEngine;
using UnityEngine.UI;

public abstract class UIScreenIdentifier : MonoBehaviour
{
    [SerializeField]
    protected RefUIBlackScreen _UIBlackScreen;
    // Can the player interact with the screen?
    public bool IsInteractable;
    // Is the screen animated?
    public bool IsAnimated;

    #region Unity Methods
    private void Awake()
    {

    }

    private void Start()
    {

    }

    private void OnEnable()
    {

    }

    private void OnDisable()
    {

    }
    #endregion

    #region Initializing Methods
    #endregion

    #region Screen Methods
    // Open screen animation.
    public virtual void OpenScreen()
    {

    }

    // Exit screen animation.
    public virtual void ExitScreen()
    {

    }

    // Selects the first desired interactable object on the screen. 
    public void SelectInitialInteractableObject()
    {

    }

    // Selects the desired interactable object on screen.
    public void SelectInteractableObject(GameObject selectableObject)
    {

    }
    #endregion
}
