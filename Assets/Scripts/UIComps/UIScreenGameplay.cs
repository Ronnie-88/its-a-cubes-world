﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScreenGameplay : UIScreenIdentifier
{
    [SerializeField]
    private RefUIManager _UIManager;
    
    public void OnPause()
    {
        Time.timeScale = 0.0f;
        PlayerInput.InvokeDisable();
        _UIManager.Value.ShowScreen<UIScreenPause>();
    }
}
