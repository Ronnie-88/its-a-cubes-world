﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    //public static UIManager Instance { get; private set; }

    private Canvas _UICanvas;
    private Dictionary<Type, UIScreenIdentifier> _UIScreens;
    private UIScreenIdentifier _CurrentUIScreen;
    private UIScreenIdentifier _PreviousUIScreen;
    public UIScreenIdentifier PreviousUIScreen { get { return _PreviousUIScreen; } }
    [SerializeField]
    private List<TutorialPromptComp> TutorialPrompts;
    [SerializeField]
    private RefUIManager _UIManager;
    #region Unity Methods
    private void Awake()
    {
        _UIManager.Value = this;
        GetUIElementComponents();
        ShowScreen<UIScreenMainMenu>();
    }
    #endregion

    #region Intitializing Methods

    private void GetUIElementComponents()
    {
        _UICanvas = GetComponent<Canvas>();

        // Create the dictionary based on the UIScreenIdentifier and a generic Type.
        _UIScreens = new Dictionary<Type, UIScreenIdentifier>();

        // For each screen in the UICanvas children add them as a generic Type to the dictionary.
        foreach (var screen in GetComponentsInChildren<UIScreenIdentifier>(true))
        {
            screen.gameObject.SetActive(false);
            _UIScreens[screen.GetType()] = screen;
        }
    }
    #endregion

    #region UI Screen Methods
    // Switch between screens using a generic type identified as a UIScreenIdentifier.
    public void ShowScreen<T>() where T : UIScreenIdentifier
    {
        // If the CurrentUIScreen is not null, set it to false before enabling the next UIScreenIdentifier.
        if (_CurrentUIScreen != null)
        {
            _PreviousUIScreen = _CurrentUIScreen;
            _CurrentUIScreen.gameObject.SetActive(false);
        }

        var varType = typeof(T);

        // Verify existence of generic inside the dictionary.
        if (_UIScreens.ContainsKey(varType) == false)
        {
            Debug.LogError($"UI Screen type of {varType} doesn't exist in the current context. Make sure that it's a child of the UICanvas.");
            return;
        }

        // Set current screen as the generic type. 
        _CurrentUIScreen = _UIScreens[varType];

        _CurrentUIScreen.gameObject.SetActive(true);
        _CurrentUIScreen.OpenScreen();
    }

    public void TutorialPromptShower(int index)
    {
        //go through a list and find the matching index and show it....
        for (int i = 0; i < TutorialPrompts.Count; i++)
        {
            if (TutorialPrompts[i].MyIndex == index)
            {
                TutorialPrompts[i].ShowPrompt();
                TutorialRemover(index);
            }
        }
    }

    public void TutorialRemover(int index)
    {
        //go through a list and find the matching index and show it....
        for (int i = 0; i < TutorialPrompts.Count; i++)
        {
            if (TutorialPrompts[i].MyIndex != index)
            {
                TutorialPrompts[i].RemovePrompt();
            }
        }
    }

    public void TutorialRemoveAll()
    {
        //go through a list and find the matching index and show it....
        for (int i = 0; i < TutorialPrompts.Count; i++)
        {
            TutorialPrompts[i].RemovePrompt();
        }
    }


    #endregion
}
