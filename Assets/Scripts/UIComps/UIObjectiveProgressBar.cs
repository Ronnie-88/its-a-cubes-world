﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIObjectiveProgressBar : MonoBehaviour
{
    [SerializeField]
    private RefObjectiveProgressBar _ObjectiveProgressBar;
    [SerializeField]
    private RefFloatAttribute _ObjectiveCount;
    private Image _ProgressBar;

    private void Awake()
    {
        _ObjectiveProgressBar.Value = this;
    }
    private void Start()
    {
        _ProgressBar = GetComponent<Image>();

    }

    public void UpdateBarOnStart()
    {
        UpdateBar(_ObjectiveCount.Value);
    }

    private void OnEnable()
    {
        _ObjectiveCount.NotifyOnValueIncreased += UpdateBar;
        _ObjectiveCount.NotifyOnValueDecreased += UpdateBar;
    }

    private void OnDisable()
    {
        _ObjectiveCount.NotifyOnValueIncreased -= UpdateBar;
        _ObjectiveCount.NotifyOnValueDecreased -= UpdateBar;
    }

    private void UpdateBar(float barValue)
    {
        _ProgressBar.fillAmount = _ObjectiveCount.GetFloatAttributePercent();
    }
}
