﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScreenMainMenu : UIScreenIdentifier
{
    [SerializeField]
    private Button _ContiueFromLastLevelButton;
    [SerializeField]
    private RefGameInstance _GameInstance;
    [SerializeField]
    private RefUIManager _UIManager;
    [SerializeField]
    private RefUIScreenMainMenu _UIScreenMainMenu;
    private void Awake()
    {
        _UIScreenMainMenu.Value = this;
        AllowContinueFromLastLevelButtonInteraction();
    }

    public void AllowContinueFromLastLevelButtonInteraction()
    {
        if (DataSerializer.DoesSaveFileExists())
        {
            _ContiueFromLastLevelButton.interactable = true;
            return;
        }
        _ContiueFromLastLevelButton.interactable = false;
    }

    public void NewGame()
    {
        _GameInstance.Value.ClearTrackers();
        DataSerializer.OverWriteSaveFileOnNewGame();
        _UIBlackScreen.Value.CrossOutFadeDefaultLoad();
        _UIManager.Value.ShowScreen<UIScreenGameplay>();
    }

    public void ContiueFromLastLevel()
    {
        _UIBlackScreen.Value.CrossFadeOutLoadGame();
        _UIManager.Value.ShowScreen<UIScreenGameplay>();
    }

    public void Options()
    {
        _UIManager.Value.ShowScreen<UIScreenOptions>();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
