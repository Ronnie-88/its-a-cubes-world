﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RawTimerNs;
public class UIScreenBlackScreen : MonoBehaviour
{
    [SerializeField]
    private float _WaitTimeDuration;
    [SerializeField]
    private RefGameInstance _GameInstance;
    [SerializeField]
    private RefUIBlackScreen _UIBlackScreen;
    [SerializeField]
    private RefUIManager _UIManager;
    [SerializeField]
    private LevelLoadManager _LevelLoadManager;
    [SerializeField]
    private CheckPointManager _CheckPointManager;
    private Animator _Animator;
    private int _FadeOutTriggerID = Animator.StringToHash("TriggerFadeOut");
    private int _FadeInTriggerID = Animator.StringToHash("TriggerFadeIn");
    private WaitForSeconds _WaitTime;
    public delegate void FunctionParameter();

    private void Awake()
    {
        _Animator = GetComponent<Animator>();
        _UIBlackScreen.Value = GetComponent<UIScreenBlackScreen>();
        _WaitTime = new WaitForSeconds(_WaitTimeDuration);
    }

    //Default fade
    public void CrossOutFadeDefaultLoad()
    {
        StartCoroutine(CrossFadeOutSequence(_LevelLoadManager.LoadNewScene));
    }

    public void PlayCrossInFade()
    {
        StartCoroutine(CrossFadeInSequence());
    }//end of default fade

    //loadgame
    public void CrossFadeOutLoadGame()
    {
        StartCoroutine(CrossFadeOutSequence(SwitchToSavedLevel));
    }
    //endloadgame

    //GameplayFades
    public void CrossFadeOutGameplay()
    {
        StartCoroutine(CrossFadeOutSequence(_CheckPointManager.ResetToCheckpointInvoke));
    }//End of game play fades

    public void CrossFadeOutBackToMainMenu()
    {
        StartCoroutine(CrossFadeOutSequence(SwitchToMainMenu));
    }

    public void LevelLoadBegin()
    {
        StartCoroutine(_LevelLoadManager.LoadingSceneAsyncSequence());
    }

    public void SwitchToMainMenu()
    {
        //Save progress here
        //_GameInstance.Value.SaveGameInstance();
        _LevelLoadManager.LoadNewScene(1);
    }
    public void CheckSwitchToCreditsScene()
    {
        if (_LevelLoadManager.CurrentLevelIndex >= 11)
        {
            _UIManager.Value.ShowScreen<UICredits>();
        }
    }

    public void SwitchToSavedLevel()
    {
        _GameInstance.Value.SwitchToSavedLevel();
    }


    private IEnumerator CrossFadeOutSequence(FunctionParameter function)
    {
        //play anim and remove any tutorial prompts
        _Animator.SetTrigger(_FadeOutTriggerID);
        _UIManager.Value.TutorialRemoveAll();
        //delay
        yield return _WaitTime;
        function();
        PlayCrossInFade();
    }

    private IEnumerator CrossFadeInSequence()
    {
        //delay
        yield return _WaitTime;
        //play anim
        _Animator.SetTrigger(_FadeInTriggerID);
    }


    private void OnEnable()
    {
        PlayerDeathHandler.NotifyOnPlayerDeath += CrossFadeOutGameplay;
        _LevelLoadManager.OnLevelLoadBegin += LevelLoadBegin;
        _LevelLoadManager.OnLevelLoadEnd += CheckSwitchToCreditsScene;
    }

    private void OnDisable()
    {
        PlayerDeathHandler.NotifyOnPlayerDeath -= CrossFadeOutGameplay;
        _LevelLoadManager.OnLevelLoadBegin -= LevelLoadBegin;
        _LevelLoadManager.OnLevelLoadEnd -= CheckSwitchToCreditsScene;
    }

}
