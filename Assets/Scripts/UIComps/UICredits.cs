﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICredits : UIScreenIdentifier
{
    [SerializeField]
    private RefUIManager _UIManager;
    [SerializeField]
    private RefSoundManager _SoundManager;
    [SerializeField]
    private RefUIScreenMainMenu _UIScreenMainMenu;

    public void BackToMainMenu()
    {
        _UIBlackScreen.Value.CrossFadeOutBackToMainMenu();
        _SoundManager.Value.PlayMainMenuTrack();
        _UIManager.Value.ShowScreen<UIScreenMainMenu>();
        Invoke("WaitForLevelLoad", 2.0f);
    }

    private void WaitForLevelLoad()
    {
        _UIScreenMainMenu.Value.AllowContinueFromLastLevelButtonInteraction();
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
