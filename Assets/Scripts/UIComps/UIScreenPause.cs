﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScreenPause : UIScreenIdentifier
{
    [SerializeField]
    private RefSoundManager _SoundManager;
    [SerializeField]
    private RefUIManager _UIManager;
    [SerializeField]
    private RefUIScreenMainMenu _UIScreenMainMenu;
    public void Resume()
    {
        Time.timeScale = 1.0f;
        PlayerInput.InvokeEnable();
        _UIManager.Value.ShowScreen<UIScreenGameplay>();
        //unpause game
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void BackToMainMenu()
    {
        //crossfade out and load mainmenu scene
        Time.timeScale = 1.0f;
        _UIBlackScreen.Value.CrossFadeOutBackToMainMenu();
        _SoundManager.Value.PlayMainMenuTrack();
        _UIManager.Value.ShowScreen<UIScreenMainMenu>();
        _UIScreenMainMenu.Value.AllowContinueFromLastLevelButtonInteraction();
        Invoke("WaitForLevelLoad", 2.0f);
    }
    private void WaitForLevelLoad()
    {
        _UIScreenMainMenu.Value.AllowContinueFromLastLevelButtonInteraction();
    }
}
