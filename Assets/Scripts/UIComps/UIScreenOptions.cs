﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScreenOptions : UIScreenIdentifier
{
    [SerializeField]
    private RefUIManager _UIManager;
    [SerializeField]
    private Button _BackToMainMenu;
    [SerializeField]
    private Button _BackToPauseMenu;

    public void BackToPauseMenu()
    {
        _UIManager.Value.ShowScreen<UIScreenPause>();
    }

    public void BackToMainMenuFromOptionScreen()
    {
        _UIManager.Value.ShowScreen<UIScreenMainMenu>();
    }


    private void OnEnable()
    {
        if (_UIManager.Value.PreviousUIScreen is UIScreenMainMenu)
        {
            _BackToMainMenu.gameObject.SetActive(true);
            _BackToPauseMenu.gameObject.SetActive(false);
        }
        else if (_UIManager.Value.PreviousUIScreen is UIScreenPause)
        {
            _BackToPauseMenu.gameObject.SetActive(true);
            _BackToMainMenu.gameObject.SetActive(false);
        }
    }


}
