using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MangerAssets/CheckPointManager")]
public class CheckPointManager : ScriptableObject
{
    //this managers the check points in the game and which one to load to on death and when game restarts
    private List<Checkpoint> Checkpoints = new List<Checkpoint>();
    public List<int> CheckpointsTriggeredIndicies = new List<int>();
    private Vector3 _PlayerPostion;
    public Vector3 PlayerPostion { get => _PlayerPostion; set => _PlayerPostion = value; }
    public event Action NotifyToResetToCheckpoint;

    public int CurrentCheckpointIndex { get; set; }

    public void AddCheckpoint(Checkpoint checkpoint)
    {
        Checkpoints.Add(checkpoint);
    }

    // public void AddTriggeredCheckPointIndex(int index)
    // {
    //     CheckpointsTriggeredIndicies.Add(index);
    // }

    // public void SaveToPlayerProfile()
    // {
    //     PlayerSaveProfile.Instance.CurrentCheckpointIndex = CurrentCheckpointIndex;
    //     Debug.Log("Saved " + PlayerSaveProfile.Instance.CurrentCheckpointIndex);
    //     Float3 float3 = new Float3(PlayerPostion.x, PlayerPostion.y, PlayerPostion.z);
    //     PlayerSaveProfile.Instance.SetNewPlayerPosition(float3);
    //     PlayerSaveProfile.Instance.SaveTriggeredCheckpointIndex(CheckpointsTriggeredIndicies);
    //     DataSerializer.SaveGameData("IWCSaveFile", PlayerSaveProfile.Instance);
    //     foreach (var item in PlayerSaveProfile.Instance.CheckPointTriggeredIndices)
    //     {
    //         Debug.Log(item);
    //     }
    // }

    // public void LoadFromPlayerProfile()
    // {
    //     Debug.Log("Load " + PlayerSaveProfile.Instance.CurrentCheckpointIndex);
    //     foreach (var item in PlayerSaveProfile.Instance.CheckPointTriggeredIndices)
    //     {
    //         Debug.Log(item);
    //     }
    //     CurrentCheckpointIndex = PlayerSaveProfile.Instance.CurrentCheckpointIndex;
    //     CheckpointsTriggeredIndicies = PlayerSaveProfile.Instance.CheckPointTriggeredIndices;
    //     Vector3 playePosition = new Vector3(PlayerSaveProfile.Instance.LastPlayerPostion.X, PlayerSaveProfile.Instance.LastPlayerPostion.Y, PlayerSaveProfile.Instance.LastPlayerPostion.Z);
    //     PlayerPostion = playePosition;
    // }


    public void ResetToCheckpointInvoke()
    {
        NotifyToResetToCheckpoint?.Invoke();
    }



    public void GetNewStartingPosition(Vector3 startingpos)
    {
        PlayerPostion = startingpos;
    }

    public void ClearCheckpoints()
    {
        CurrentCheckpointIndex = 0;
        Checkpoints.Clear();
        CheckpointsTriggeredIndicies.Clear();
    }

}
