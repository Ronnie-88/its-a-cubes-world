using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "MangerAssets/LevelLoadManager")]
public class LevelLoadManager : ScriptableObject
{
    //grants ability to a client to load a requested level
    //we need a way to call request a level change
    public event Action<int> OnSwitchAudioTrack;
    public event Action OnLevelLoadBegin;
    public event Action OnLevelLoadEnd;
    private const int MAXGAMEPLAYLEVELCOUNT = 10;//game play level index anything above is handled separately
    public int CurrentLevelIndex { get; private set; }
    private AsyncOperation _LevelToLoadOperation;
    private Scene _LevelToUnLoad;
    private Scene _NewActiveScene;
    private string _PreloadSceneName = "PRELOAD";

    //******************************************************************************
    public void LoadNewScene()
    {
        CurrentLevelIndex = SceneManager.GetActiveScene().buildIndex;
        //add scene to unload
        AddScenesToUnload();
        //set the new scene to load
        AddSceneToLoad((++CurrentLevelIndex));
        _NewActiveScene = SceneManager.GetSceneByBuildIndex(CurrentLevelIndex);
        OnLevelLoadBegin?.Invoke();
        OnSwitchAudioTrack?.Invoke(CurrentLevelIndex);
    }

    public void LoadNewScene(int levelIndex)
    {
        AddScenesToUnload();
        AddSceneToLoad(levelIndex);
        _NewActiveScene = SceneManager.GetSceneByBuildIndex(levelIndex);
        OnLevelLoadBegin?.Invoke();
        OnSwitchAudioTrack?.Invoke(levelIndex);
    }

    private void AddSceneToLoad(int index)
    {
        if (!IsSceneLoaded(index))
        {
            _LevelToLoadOperation = SceneManager.LoadSceneAsync(index, LoadSceneMode.Additive);
        }
    }

    private void SetNewActiveScene(AsyncOperation operation)
    {
        SceneManager.SetActiveScene(_NewActiveScene);
        OnLevelLoadEnd?.Invoke();
    }

    private void AddScenesToUnload()
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.name != _PreloadSceneName)
            {
                _LevelToUnLoad = scene;
            }
        }


    }

    private bool IsSceneLoaded(int index)
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.buildIndex == index)
            {
                return true;
            }
        }

        return false;
    }

    public IEnumerator LoadingSceneAsyncSequence()
    {
        while (!_LevelToLoadOperation.isDone)
        {
            yield return null;
        }
        _LevelToLoadOperation.completed += SetNewActiveScene;
        //clear load level operation
        _LevelToLoadOperation = null;
        UnloadLevelLastLevel();
    }

    private void UnloadLevelLastLevel()
    {
        if (!_LevelToUnLoad.IsValid())
        {
            return;
        }
        SceneManager.UnloadSceneAsync(_LevelToUnLoad);
    }

    //*******************************************************

    public void SaveToPlayerProfile()
    {
        if (CurrentLevelIndex >= 2 && CurrentLevelIndex < 11)
        {
            PlayerSaveProfile.Instance.CurrentLevelIndex = CurrentLevelIndex;
        }
    }


    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }
}
