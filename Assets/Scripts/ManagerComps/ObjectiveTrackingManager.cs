using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MangerAssets/ObjectiveTrackingManager")]
public class ObjectiveTrackingManager : ScriptableObject
{
    //this class is responsible for keeping track of the number of cubes the player has reached
    //and granting access to switch levels if objective was reached.
    [SerializeField]
    private RefFloatAttribute _KeyCubeCount;
    private List<KeyCube> LevelKeyCubes= new List<KeyCube>();
    // private List<int> LevelKeycubeTriggeredIndices = new List<int>();
    public event Action NotifyOnUpdateCurrentObjective;
    public event Action NotifyTrackProgress;

    //try to call this 0.2 seconds after game starts to give time for cubes to add themselves
    public void InitializeCurrentObjective()
    {
        _KeyCubeCount.Value = 0.0f;
        _KeyCubeCount.Max = (float)LevelKeyCubes.Count;
        _KeyCubeCount.Min = 0.0f;
    }

    public void RemoveKeyCubes()
    {
        LevelKeyCubes.Clear();
    }

    // public void LoadFromPlayerProfile()
    // {
    //     LevelKeycubeTriggeredIndices = PlayerSaveProfile.Instance.KeyCubesTriggeredIndices;
    //     _KeyCubeCount.Max = PlayerSaveProfile.Instance.LevelKeyCountMax;
    //     _KeyCubeCount.Value = PlayerSaveProfile.Instance.CurrentLevelKeyCubesReached;
    //     _KeyCubeCount.Min = 0.0f;
    // }

    // public void SaveToPlayerProfile()
    // {
    //     PlayerSaveProfile.Instance.LevelKeyCountMax = _KeyCubeCount.Max;
    //     PlayerSaveProfile.Instance.SaveTriggeredKeyCubeIndex(LevelKeycubeTriggeredIndices);
    //     PlayerSaveProfile.Instance.CurrentLevelKeyCubesReached = _KeyCubeCount.Value;
    // }

    //this should be called when the player spawns or when the players steps on a keycube
    public bool IsObjectiveReached()
    {
        return _KeyCubeCount.Value >= _KeyCubeCount.Max;
    }

    // public bool GetKeyIndexAt(int index)
    // {
    //     for (int i = 0; i < LevelKeycubeTriggeredIndices.Count; i++)
    //     {
    //         if (index == LevelKeycubeTriggeredIndices[i])
    //         {
    //             return true;
    //         }
    //     }
    //     return false;
    // }

    private void UpdateCurrentObjective()
    {
        _KeyCubeCount.Value++;
        NotifyTrackProgress?.Invoke();
    }

    public void UpdateCurrentObjectiveInvoker()
    {
        NotifyOnUpdateCurrentObjective?.Invoke();
    }

    public void AddKeyCube(KeyCube keyCube)
    {
        LevelKeyCubes.Add(keyCube);
    }


    // public void AddKeyCubeTriggeredIndex(int index)
    // {
    //     LevelKeycubeTriggeredIndices.Add(index);
    // }

    // public void RemoveLevelKeyCubeIndices()
    // {
    //     LevelKeycubeTriggeredIndices.Clear();
    // }


    private void OnEnable()
    {
        NotifyOnUpdateCurrentObjective += UpdateCurrentObjective;
    }
    private void OnDisable()
    {
        NotifyOnUpdateCurrentObjective -= UpdateCurrentObjective;
    }


}
