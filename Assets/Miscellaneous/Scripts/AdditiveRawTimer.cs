﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace RawTimerNs
{
    [System.Serializable]
    public class AdditiveRawTimer
    {
        public event Action NotifyOnTick;
        public event Action NotifyOnTimerEnd;
        public event Action NotifyOnTimerStart;

        public float CurrentTime { get; private set; }
        public float Duration { get; private set; }

        public void StartTimer(float duration)
        {
            Duration = duration;
            CurrentTime = 0.0f;
            NotifyOnTimerStart?.Invoke();
        }

        public void TimerTick(float deltaTime)
        {
            if (CurrentTime == Duration) { return; }

            CurrentTime += deltaTime;
            NotifyOnTick?.Invoke();
            CheckForTimerEnd();
        }

        public void OverrideTimer()
        {
            CurrentTime = Duration;
            NotifyOnTimerEnd?.Invoke();
        }

        private void CheckForTimerEnd()
        {
            if (CurrentTime < Duration) { return; }
            CurrentTime = Duration;
            NotifyOnTimerEnd?.Invoke();
        }
    }
}
