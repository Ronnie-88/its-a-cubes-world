﻿using System;
namespace RawTimerNs
{
    [System.Serializable]
    public class SubtractiveRawTimer
    {
        public event Action NotifyOnTick;
        public event Action NotifyOnTimerEnd;
        public event Action NotifyOnOverride;
        public event Action NotifyOnTimerStart;

        public float CurrentTime { get; private set; }


        public void StartTimer(float duration)
        {
            CurrentTime = duration;
            NotifyOnTimerStart?.Invoke();
        }

        public void TimerTick(float deltaTime)
        {
            if (CurrentTime == 0.0f) { return; }

            CurrentTime -= deltaTime;
            NotifyOnTick?.Invoke();
            CheckForTimerEnd();
        }

        public void OverrideTimer()
        {
            CurrentTime = 0.0f;
            NotifyOnOverride?.Invoke();
        }

        private void CheckForTimerEnd()
        {
            if (CurrentTime > 0.0f) { return; }

            CurrentTime = 0.0f;
            NotifyOnTimerEnd?.Invoke();
        }
    }

}
